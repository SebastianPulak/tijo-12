package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

public class CSVtoJsonReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);
    List<MovieDto> movies = new ArrayList<>();

    public List<MovieDto> CSVJsonReader() {
        try (
                CSVReader csvReader = new CSVReader(new FileReader("movies.csv"), ';')) {
            String[] values = null;
            csvReader.readNext();
            while ((values = csvReader.readNext()) != null) {
                movies.add(new MovieDto(Integer.parseInt(values[0]), values[1], Integer.parseInt(values[2]), values[3]));
                values = null;
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return movies;
    }

    public void writeToCSV() {
        try (
                Writer writer = Files.newBufferedWriter(Paths.get("./movies.csv"));

                CSVWriter csvWriter = new CSVWriter(writer,
                        CSVWriter.DEFAULT_SEPARATOR,
                        CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
        ){
            InputStream is = new FileInputStream(new File("F:\\Uni\\tijo-12\\Movie\\movie.json"));

            // Create JsonReader from Json.
            JsonReader reader = Json.createReader(is);

            // Get the JsonObject structure from JsonReader.
            JsonObject obj = reader.readObject();
            reader.close();
            String id = Integer.toString(obj.getInt("movieId"));
            String title = obj.getString("title");
            String year = Integer.toString(obj.getInt("year"));
            String image = obj.getString("image");

            String line = id+";"+title+";"+year+";"+image+";";
            csvWriter.writeNext(new String[]{line});

        } catch (Exception e) {
            System.out.println("Failed: " + e.getMessage());
        }

    }
}

